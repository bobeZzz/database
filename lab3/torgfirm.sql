/* task1
declare @dateUntil date = '2017-07-02'
select * from zakaz
	inner join zakaz_tovar on zakaz_tovar.id_tovar = Zakaz.id_zakaz
	inner join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
	where Zakaz.Date_naznach < @dateUntil
	*/

/*task2
declare @lPrice money = 18, @uPrice money = 30
select * from tovar
	where tovar.NaSklade>14 and 
	price between @lPrice and @uPrice
*/

/* task3 
select * from zakaz
	where date_naznach is null
--or
select * from zakaz
	where date_rozm is null
	*/

/*task4
declare @postachaknikName varchar(50) = '��� "����"'
select * from tovar 
	inner join postachalnik on tovar.id_postav = postachalnik.id_postach
	where postachalnik.Nazva = @postachaknikName
	*/

	/*task5
declare @todayDate date = '2017-07-10'
select sum(zakaz_tovar.Kilkist*tovar.Price) from zakaz 
	inner join zakaz_tovar on zakaz.id_zakaz = zakaz_tovar.id_tovar
	inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
	inner join postachalnik on postachalnik.id_postach = tovar.id_postav
	where zakaz.date_naznach between dateadd(day, -30, @todayDate) and @todayDate
	and postachalnik.Nazva like '���%'*/
	
	/*task6
declare @clientId int = 1
select id_klient,date_naznach from zakaz 
	where zakaz.id_klient = @clientId
	*/

	/*task7
	select postachalnik.Nazva from zakaz
 inner join zakaz_tovar on zakaz.id_zakaz = zakaz_tovar.id_tovar
 inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
right join postachalnik on postachalnik.id_postach = tovar.id_postav
 where postachalnik.Nazva like '���%'
 and (select count(*) from tovar 
  right join postachalnik on tovar.id_postav = postachalnik.id_postach
  where postachalnik.Nazva like '���%') <> 0
	*/

	/*task8
declare @todayDate date = '2017-07-10'
select  klient.Nazva from zakaz 
	inner join zakaz_tovar on zakaz.id_zakaz = zakaz_tovar.id_tovar
	inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
	inner join klient on klient.id_klient = zakaz.id_klient
	where zakaz.date_naznach between dateadd(day, -30, @todayDate) and @todayDate
	and klient.Nazva like '��%'
	group by klient.Nazva*/

	/*task9
	select * from sotrudnik
	where Name='�����'
	order by Fname asc*/

	/*task10
	select * from klient 
	where e-mail is not null
	order by id_klient*/